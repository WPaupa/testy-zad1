#include <assert.h>

int polynomial_degree(const int *y, int n);

int main() {
    {
        int y[4] = {2, 1, 3, 7};
        assert(polynomial_degree(y, 4) == 3);
    }
    {
        int y[1] = {0};
        assert(polynomial_degree(y, 1) == -1);
    }
    {
        int y[2] = {4, 1};
        assert(polynomial_degree(y, 2) == 1);
    }
    {
        int y[9] = {-2, -1, 0, 1, 2, 3, 4, 5, 6};
        assert(polynomial_degree(y, 9) == 1);
    }
}
