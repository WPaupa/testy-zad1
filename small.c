#include <assert.h>

int polynomial_degree(const int *y, int n);

int main() {
    {
        int y[1] = {69};
        assert(polynomial_degree(y, 1) == 0);
    }
    {
        int y[1] = {0};
        assert(polynomial_degree(y, 1) == -1);
    }
    {
        int y[2] = {2, 4};
        assert(polynomial_degree(y, 2) == 1);
    }
    {
        int y[2] = {0, 0};
        assert(polynomial_degree(y, 2) == -1);
    }
    {
        int y[3] = {0, 0, 0};
        assert(polynomial_degree(y, 3) == -1);
    }
    {
        int y[3] = {60, 60, 60};
        assert(polynomial_degree(y, 3) == 0);
    }
    {
        int y[3] = {63, 63, 63};
        assert(polynomial_degree(y, 3) == 0);
    }
    {
        int y[3] = {0, 0, 0};
        assert(polynomial_degree(y, 3) == -1);
    }
    {
        int y[3] = {28, 28, 28};
        assert(polynomial_degree(y, 3) == 0);
    }
    {
        int y[3] = {16, 16, 16};
        assert(polynomial_degree(y, 3) == 0);
    }
    {
        int y[4] = {66, 44, 22, 0};
        assert(polynomial_degree(y, 4) == 1);
    }
    {
        int y[4] = {12, 12, 12, 12};
        assert(polynomial_degree(y, 4) == 0);
    }
    {
        int y[4] = {51, 42, 33, 24};
        assert(polynomial_degree(y, 4) == 1);
    }
    {
        int y[4] = {14, 15, 16, 17};
        assert(polynomial_degree(y, 4) == 1);
    }
    {
        int y[4] = {32, 35, 35, 38};
        assert(polynomial_degree(y, 4) == 3);
    }
    {
        int y[4] = {30, 21, 12, 3};
        assert(polynomial_degree(y, 4) == 1);
    }
    {
        int y[4] = {25, 34, 43, 52};
        assert(polynomial_degree(y, 4) == 1);
    }
    {
        int y[4] = {35, 41, 47, 53};
        assert(polynomial_degree(y, 4) == 1);
    }
    {
        int y[4] = {51, 34, 17, 0};
        assert(polynomial_degree(y, 4) == 1);
    }
    {
        int y[4] = {53, 37, 21, 5};
        assert(polynomial_degree(y, 4) == 1);
    }
    {
        int y[5] = {61, 32, 12, 17, 63};
        assert(polynomial_degree(y, 5) == 3);
    }
    {
        int y[5] = {19, 7, 37, 53, -1};
        assert(polynomial_degree(y, 5) == 3);
    }
    {
        int y[5] = {15, 26, 33, 36, 35};
        assert(polynomial_degree(y, 5) == 2);
    }
    {
        int y[5] = {25, 32, 46, 44, 3};
        assert(polynomial_degree(y, 5) == 3);
    }
    {
        int y[5] = {24, 22, 28, 42, 64};
        assert(polynomial_degree(y, 5) == 2);
    }
    {
        int y[5] = {-2, 13, 19, 16, 4};
        assert(polynomial_degree(y, 5) == 2);
    }
    {
        int y[5] = {23, 66, 66, 38, 12};
        assert(polynomial_degree(y, 5) == 4);
    }
    {
        int y[5] = {10, 26, 30, 22, 2};
        assert(polynomial_degree(y, 5) == 2);
    }
    {
        int y[5] = {9, 44, 62, 63, 47};
        assert(polynomial_degree(y, 5) == 2);
    }
    {
        int y[5] = {25, 30, 35, 40, 45};
        assert(polynomial_degree(y, 5) == 1);
    }
    {
        int y[6] = {38, 57, 53, 25, 2, 43};
        assert(polynomial_degree(y, 6) == 4);
    }
    {
        int y[6] = {29, 3, 13, 38, 57, 49};
        assert(polynomial_degree(y, 6) == 3);
    }
    {
        int y[6] = {14, 28, 57, 59, 37, 39};
        assert(polynomial_degree(y, 6) == 4);
    }
    {
        int y[6] = {10, 19, 23, 25, 24, 15};
        assert(polynomial_degree(y, 6) == 4);
    }
    {
        int y[6] = {4, 31, 39, 23, 2, 19};
        assert(polynomial_degree(y, 6) == 4);
    }
    {
        int y[6] = {48, 27, 29, 37, 34, 3};
        assert(polynomial_degree(y, 6) == 3);
    }
    {
        int y[6] = {35, 19, 44, 38, 3, 15};
        assert(polynomial_degree(y, 6) == 4);
    }
    {
        int y[6] = {58, 33, 52, 41, 12, 63};
        assert(polynomial_degree(y, 6) == 4);
    }
    {
        int y[6] = {2, 38, 57, 59, 44, 12};
        assert(polynomial_degree(y, 6) == 2);
    }
    {
        int y[6] = {52, 29, 3, 18, 61, 62};
        assert(polynomial_degree(y, 6) == 4);
    }
    {
        int y[7] = {-2, 10, 14, 25, 44, 56, 28};
        assert(polynomial_degree(y, 7) == 5);
    }
    {
        int y[7] = {21, 47, -1, -2, 40, 64, 20};
        assert(polynomial_degree(y, 7) == 5);
    }
    {
        int y[7] = {40, 61, 56, 39, 24, 25, 56};
        assert(polynomial_degree(y, 7) == 3);
    }
    {
        int y[7] = {54, 7, 61, 56, 36, 66, 49};
        assert(polynomial_degree(y, 7) == 5);
    }
    {
        int y[7] = {40, 26, 42, 47, 28, 0, 6};
        assert(polynomial_degree(y, 7) == 4);
    }
    {
        int y[7] = {3, 21, 30, 44, 40, 12, 25};
        assert(polynomial_degree(y, 7) == 5);
    }
    {
        int y[7] = {26, 46, 1, 7, 44, 56, 51};
        assert(polynomial_degree(y, 7) == 5);
    }
    {
        int y[7] = {34, 28, 35, 28, 16, 13, 7};
        assert(polynomial_degree(y, 7) == 5);
    }
    {
        int y[7] = {8, 43, 57, 28, 20, 63, 33};
        assert(polynomial_degree(y, 7) == 5);
    }
    {
        int y[7] = {66, 30, -2, 31, 58, 20, 14};
        assert(polynomial_degree(y, 7) == 5);
    }
    {
        int y[8] = {20, 58, 57, 31, 9, 23, 64, 6};
        assert(polynomial_degree(y, 8) == 6);
    }
    {
        int y[8] = {59, 41, 31, 36, 32, 22, 34, 59};
        assert(polynomial_degree(y, 8) == 6);
    }
    {
        int y[8] = {49, 18, 43, 14, 7, 25, 0, 56};
        assert(polynomial_degree(y, 8) == 6);
    }
    {
        int y[8] = {43, 6, 30, 12, 4, 23, 19, 1};
        assert(polynomial_degree(y, 8) == 6);
    }
    {
        int y[8] = {46, 35, 37, 16, 12, 20, 2, 32};
        assert(polynomial_degree(y, 8) == 6);
    }
    {
        int y[8] = {39, 25, 46, 1, 9, 62, 28, 4};
        assert(polynomial_degree(y, 8) == 6);
    }
    {
        int y[8] = {14, 45, 38, 28, 48, 62, 19, 28};
        assert(polynomial_degree(y, 8) == 6);
    }
    {
        int y[8] = {26, 29, 6, 48, 64, 27, 14, 40};
        assert(polynomial_degree(y, 8) == 6);
    }
    {
        int y[8] = {1, 61, 43, 15, 32, 57, 26, 57};
        assert(polynomial_degree(y, 8) == 6);
    }
    {
        int y[8] = {57, 34, 30, 39, 37, 31, 42, 22};
        assert(polynomial_degree(y, 8) == 6);
    }
    {
        int y[9] = {42, 52, 13, 26, 31, 29, 45, 43, 4};
        assert(polynomial_degree(y, 9) == 7);
    }
    {
        int y[9] = {51, 41, 28, 20, 2, 16, 55, 28, 53};
        assert(polynomial_degree(y, 9) == 7);
    }
    {
        int y[9] = {4, 18, 58, 36, 8, 25, 55, 55, 6};
        assert(polynomial_degree(y, 9) == 8);
    }
    {
        int y[9] = {60, 4, 53, 13, 1, 12, 10, 64, 50};
        assert(polynomial_degree(y, 9) == 7);
    }
    {
        int y[9] = {29, 40, 1, 23, 45, 39, 41, -1, 29};
        assert(polynomial_degree(y, 9) == 8);
    }
    {
        int y[9] = {27, 20, 53, 8, 9, 14, 23, 51, 55};
        assert(polynomial_degree(y, 9) == 8);
    }
    {
        int y[9] = {35, 20, 23, 15, 37, 57, 57, 65, 57};
        assert(polynomial_degree(y, 9) == 8);
    }
    {
        int y[9] = {56, 26, 22, 13, 28, 43, 33, 52, 4};
        assert(polynomial_degree(y, 9) == 8);
    }
    {
        int y[9] = {26, -1, 4, 16, 20, 24, 27, 55, -2};
        assert(polynomial_degree(y, 9) == 8);
    }
    {
        int y[9] = {54, 25, 14, 14, 1, 23, 33, 6, 19};
        assert(polynomial_degree(y, 9) == 8);
    }
    {
        int y[10] = {-1, 62, 60, 18, 63, 51, -1, 51, 36, 21};
        assert(polynomial_degree(y, 10) == 9);
    }
    {
        int y[10] = {26, 13, 48, 64, 49, 1, -2, 62, 11, 8};
        assert(polynomial_degree(y, 10) == 8);
    }
    {
        int y[10] = {60, 57, 30, 13, 8, 8, 16, 43, 64, 47};
        assert(polynomial_degree(y, 10) == 9);
    }
    {
        int y[10] = {38, 59, 24, 50, 51, 19, 20, 62, 44, 47};
        assert(polynomial_degree(y, 10) == 8);
    }
    {
        int y[10] = {55, 31, 34, -2, 32, 52, 24, 23, 24, 52};
        assert(polynomial_degree(y, 10) == 8);
    }
    {
        int y[10] = {43, 38, 6, 21, 35, 34, 22, 4, 9, 64};
        assert(polynomial_degree(y, 10) == 8);
    }
    {
        int y[10] = {52, 30, 3, 10, 27, 42, 24, 6, 0, 31};
        assert(polynomial_degree(y, 10) == 9);
    }
    {
        int y[10] = {66, 54, 54, 23, 11, 0, 18, 42, -2, 4};
        assert(polynomial_degree(y, 10) == 9);
    }
    {
        int y[10] = {22, 12, 39, 29, 38, 36, 35, 65, 34, 40};
        assert(polynomial_degree(y, 10) == 8);
    }
    {
        int y[10] = {25, 55, 21, 18, 6, 0, 25, 56, 48, 46};
        assert(polynomial_degree(y, 10) == 8);
    }
}
